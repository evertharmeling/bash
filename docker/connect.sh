#! /bin/bash

VAGRANT_CWD=~/projects/
DOCKER_NAME="dockerdev_dev_1"
printf "Starting Vagrant & Docker (%s) from %s...\n" "$DOCKER_NAME" "$VAGRANT_CWD"


# Check if Vagrant is running, if not start it
VAGRANT_STATUS=$(VAGRANT_CWD=$VAGRANT_CWD vagrant status boot2docker | grep boot2docker | awk '{print $2}')
if [ "$VAGRANT_STATUS" != "running" ]; then
    printf "Vagrant is not running, starting now...\n"
    VAGRANT_CWD="$VAGRANT_CWD" vagrant up || exit 1
fi


# Check if Docker is running, if not start it
DOCKER_STATUS=$(VAGRANT_CWD=$VAGRANT_CWD vagrant ssh -c "docker ps -a -f name="$DOCKER_NAME" --format {{.Status}}" -- -A | grep -v "Using nfs2 synced folder option")
if [[ "$DOCKER_STATUS" != "Up"* ]]; then
    printf "Docker (%s) is not running, starting now...\n" "$DOCKER_NAME"
    VAGRANT_CWD="$VAGRANT_CWD" vagrant ssh -c "docker-compose -f '$VAGRANT_CWD/docker-dev/docker-compose.yml' up -d" -- -A || exit 2
fi


# Connect through Vagrant into Docker
VAGRANT_CWD="$VAGRANT_CWD" vagrant ssh -c "docker exec -ti $DOCKER_NAME /bin/bash" -- -A
