#! /bin/bash

source "${BASH_SOURCE%/*}/../config.sh"

[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"

ADDRESS=$DOCKER_ADDRESS
USERNAME=$DOCKER_USERNAME

if [[ -z $USERNAME && -n $SUDO_USER ]]; then
    USERNAME=$SUDO_USER
elif [[ -z $USERNAME ]]; then
    echo "No username was set, and \$SUDO_USER is empty"
    echo "Please set a username or run this script with sudo"
    exit 3
fi

if [[ $# -lt 1 ]]; then
    echo "Please specify a vhost name"
    exit 1
fi

if [[ -w /etc/hosts ]]; then
    # Strip EOF newlines and add one extra newline
    sed -i '' -e :a -e '/^\n*$/{$d;N;};/\n$/ba' /etc/hosts
    echo "" >> /etc/hosts

    arr=$@
    if [ $# -eq 1 ]; then
        arr=($1 www.$1)
    fi

    for j in ${arr[@]}; do
	    if grep -q $j.$USERNAME.local "/etc/hosts"; then
            echo "! $j.$USERNAME.local exists already"
	    else
    	    echo "$ADDRESS	$j.$USERNAME.local" >> /etc/hosts
	        echo "Added alias $j.$USERNAME.local for $ADDRESS";
    	fi
    done
else
    echo "/etc/hosts is not writable! Are you root?"
    exit 2
fi
