# Bash
Aliases, scripts, PS1, etc. for Bash

## Installation
Clone this repository (to `~/.bash/` for example) and run `~/.bash/install.sh`

## Configuration
Create a `config.sh` file in the root of this directory with the following contents:

```bash
#! /bin/bash

# Git
GIT_USERNAME=""
GIT_ORGANIZATION=""

# Docker
DOCKER_ADDRESS=""
DOCKER_USERNAME=""
```
