#! /bin/bash

PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
echo -e "source \"$PWD/bash.sh\"\n\n$(cat ~/.bash_profile)" > ~/.bash_profile

echo "Run 'source ~/.bash_profile' in this terminal to enable your new setup ;)"
