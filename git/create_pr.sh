#! /bin/bash

source "${BASH_SOURCE%/*}/../config.sh"

BRANCH=$(git symbolic-ref --short -q HEAD)
REMOTE_BRANCH=$(git remote show upstream | grep 'HEAD branch:' | cut -f2- -d':' | xargs echo)
REPO=$(basename $(git rev-parse --show-toplevel))

IFS='/' read -ra ADDR <<< "$BRANCH"
ID=${ADDR[1]}

git push origin "$BRANCH"
open "https://github.com/$GIT_ORGANIZATION/$REPO/compare/$REMOTE_BRANCH...$GIT_USERNAME:$BRANCH?expand=1&title=$ID"
