#! /bin/bash

REMOTE_BRANCH=$(git remote show upstream | grep 'HEAD branch:' | cut -f2- -d':' | xargs echo)
git pull --rebase upstream "$REMOTE_BRANCH"
