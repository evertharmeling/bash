#! /bin/bash

# Check if a branch exists and just switch to it if it exists already
if [ `git branch --list $1` ]; then
    echo "Branch $1 exists already, just checking out"
    git checkout $1
    exit 1
fi

# Create new branch and setup remote
git checkout -b $1
git config branch.$1.remote origin
git config branch.$1.merge refs/heads/$1
git config branch.$1.rebase true
