#! /bin/bash

source "${BASH_SOURCE%/*}/../config.sh"

git clone "git@github.com:$GIT_USERNAME/$1.git" || exit 0
cd "./$1"
git remote add upstream "git@github.com:$GIT_ORGANIZATION/$1.git"

echo
echo "Adding entries to /etc/hosts..."
"${BASH_SOURCE%/*}/../misc/hosts_add.sh" "$1"
