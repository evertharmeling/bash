#! /bin/bash

# Bash
alias ls="ls -alG"
alias sudo="sudo "

# GIT
alias gs="git status"
alias gl="git log --oneline --all --graph --decorate $*"
alias gaa="git add -A :/ && git status"
alias gc="git commit -m $*"
alias gca="git commit --amend --no-edit"
alias gp="git push origin"
alias clone="${BASH_SOURCE%/*}/git/clone.sh"
alias branch="${BASH_SOURCE%/*}/git/branch.sh"
alias rebase="${BASH_SOURCE%/*}/git/rebase.sh"
alias createpr="${BASH_SOURCE%/*}/git/create_pr.sh"

source ${BASH_SOURCE%/*}/git/completion.sh

# Miscellaneous
alias addvhost="${BASH_SOURCE%/*}/misc/hosts_add.sh"

# Jira
alias jira="${BASH_SOURCE%/*}/jira/branch.sh"
alias jiraclean="${BASH_SOURCE%/*}/jira/branch_cleanup.sh"
alias gcj="${BASH_SOURCE%/*}/jira/commit.sh"

# Docker
alias dcon="${BASH_SOURCE%/*}/docker/connect.sh"

# Colors
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# Functions
title() {
    echo -ne "\033]0;"$*"\007"
}

# PS1
parse_git_branch() {
    git branch 2> /dev/null | sed -e "/^[^*]/d" -e "s/* \(.*\)/(\1)/"
}

export PS1="\[\033[01;30m\]\n[\t] \[\033[00;37m\]\u\[\033[01;30m\]@\[\033[00;37m\]\H \[\033[00;32m\]\w \[\033[00;33m\]\$(parse_git_branch)\[\033[00;37m\] \n\$ \[\033[00m\]"
