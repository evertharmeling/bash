#! /bin/bash

# Show message
echo "This will delete the following branches:"
git branch | grep "\s\?jira/" | while read branch; do
    echo "$branch"
done

echo
read -r -p "Are you sure you want to delete these branches (both local and remote on origin)? [y/N] " response
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
    git branch | grep "\s\?jira/" | while read branch; do
        echo "Deleting (local & remote) branch: $branch"
        git branch -d "$branch" && git push origin --delete "$branch"
    done
fi
