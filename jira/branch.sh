#! /bin/bash

BRANCH="jira/$1"
REMOTE_BRANCH=$(git remote show upstream | grep 'HEAD branch:' | cut -f2- -d':' | xargs echo)

if [[ $(git branch --list $BRANCH) ]]; then
    echo "Branch '$BRANCH' exists already, just checking out..."
    git checkout $BRANCH

    echo
    read -r -p "Do you want to rebase upstream/$REMOTE_BRANCH? [y/N] " response
    if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
        git pull --rebase upstream "$REMOTE_BRANCH"
    fi
else
    echo "Branch '$BRANCH' doesn't exist yet, creating now..."
    "${BASH_SOURCE%/*}/../git/branch.sh" "$1"

    echo
    read -r -p "Do you want to reset this branch to upstream/$REMOTE_BRANCH? [y/N] " response
    if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
        git remote update
        git reset --hard "upstream/$REMOTE_BRANCH" --
    fi
fi
