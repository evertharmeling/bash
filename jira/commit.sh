#! /bin/bash

BRANCH=$(git symbolic-ref --short -q HEAD)

if [[ $BRANCH == *"jira/"* ]]; then
    IFS='/' read -ra ADDR <<< "$BRANCH"
    ID=${ADDR[1]}

    git commit -m "$ID $1"
else
    echo "You're not a JIRA-issue branch!"
fi
